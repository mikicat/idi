#include "MyGLWidget.h"
#include <iostream>
#include <stdio.h>

#define printOpenGLError() printOglError(__FILE__, __LINE__)
#define CHECK() printOglError(__FILE__, __LINE__,__FUNCTION__)
#define DEBUG() std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::endl;

int MyGLWidget::printOglError(const char file[], int line, const char func[])
{
    GLenum glErr;
    int    retCode = 0;

    glErr = glGetError();
    const char * error = 0;
    switch (glErr)
    {
        case 0x0500:
            error = "GL_INVALID_ENUM";
            break;
        case 0x501:
            error = "GL_INVALID_VALUE";
            break;
        case 0x502:
            error = "GL_INVALID_OPERATION";
            break;
        case 0x503:
            error = "GL_STACK_OVERFLOW";
            break;
        case 0x504:
            error = "GL_STACK_UNDERFLOW";
            break;
        case 0x505:
            error = "GL_OUT_OF_MEMORY";
            break;
        default:
            error = "unknown error!";
    }
    if (glErr != GL_NO_ERROR)
    {
        printf("glError in file %s @ line %d: %s function: %s\n",
                             file, line, error, func);
        retCode = 1;
    }
    return retCode;
}

void MyGLWidget::iniCamera() {
  obs = glm::vec3(0.5, 2, 12);
  vrp = glm::vec3(0.5, 0, 0.5);
  up = glm::vec3(0, 1, 0);
  fov = float(M_PI)/3.2f; // FOV canviat per augmentar angle d'obertura
  ra  = 1.0;
  znear =  6.5;
  zfar  = 20; // zfar canviat

  viewTransform();
  projectTransform();
}

void MyGLWidget::paintGL() {
  // descomentar per canviar paràmetres
  // glViewport (0, 0, ample, alt);

  // Esborrem el frame-buffer
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Homer
  homerTransform(xH, zH);
  glBindVertexArray (VAO_Homer);
  glUniform1i(indexColorLoc, 2);  // color = 2 -- color per vèrtex
  glDrawArrays(GL_TRIANGLES, 0, homer.faces().size()*3);
  glBindVertexArray (0);

  // Escac
  int cell_color = 1;
  for (int i = -4; i < 4; ++i) {
    for (int j = -4; j < 4; ++j) {
      escacTransform(i+1, j+1);
      glBindVertexArray (VAO_Escac);
      glUniform1i(indexColorLoc, (++cell_color%2));  // color = 1 -> blanc / = 0 -> negre
      glDrawArrays(GL_TRIANGLES, 0, 6);
      glBindVertexArray (0);
    }
    ++cell_color;
  }
}

void MyGLWidget::keyPressEvent (QKeyEvent *event) {
  makeCurrent();
  int old_x = xH;
  int old_z = zH;
  int old_a = angleH;
  switch (event->key()) {
    case Qt::Key_1:
      xH += deltai[0];
      zH += deltaj[0];
      angleH = 180*float(M_PI)/180;
      break;
    case Qt::Key_2:
      xH += deltai[1];
      zH += deltaj[1];
      angleH = -90*float(M_PI)/180;
      break;
    case Qt::Key_3:
      xH += deltai[2];
      zH += deltaj[2];
      angleH = -90*float(M_PI)/180;
      break;
    case Qt::Key_4:
      xH += deltai[3];
      zH += deltaj[3];
      angleH = 0;
      break;
    case Qt::Key_5:
      xH += deltai[4];
      zH += deltaj[4];
      angleH = 0;
      break;
    case Qt::Key_6:
      xH += deltai[5];
      zH += deltaj[5];
      angleH = 90*float(M_PI)/180;
      break;
    case Qt::Key_7:
      xH += deltai[6];
      zH += deltaj[6];
      angleH = 90*float(M_PI)/180;
      break;
    case Qt::Key_8:
      xH += deltai[7];
      zH += deltaj[7];
      angleH = float(M_PI);
      break;
    case Qt::Key_C:
      if (cam == 1) {
        obs = glm::vec3(0.5, 8, 0.5);
        up = glm::vec3(0, 0, -1);
      }
      else {
        obs = glm::vec3(0.5, 2, 12);
        up = glm::vec3(0, 1, 0);
      }
      projectTransform();
      viewTransform();
      cam = -cam;
      break;
    default: event->ignore(); break;
  }
  if (xH <= -4 || xH > 4 || zH <= -4 || zH > 4) {
    xH = old_x, zH = old_z, angleH = old_a;
  }
  if (xH == -3) {
    angleH = 90*float(M_PI)/180;
  } else if (xH == 4) {
    angleH = -90*float(M_PI)/180;
  } else if (zH == -3) {
    angleH = 0;
  } else if (zH == 4) {
    angleH = float(M_PI);
  }

  update();
}

void MyGLWidget::projectTransform() {
  glm::mat4 Proj(1.0f);
  Proj = glm::perspective (fov, ra, znear, zfar);
  glUniformMatrix4fv (projLoc, 1, GL_FALSE, &Proj[0][0]);
}

void MyGLWidget::viewTransform() {
  glm::mat4 View(1.0f);
  View = glm::lookAt (obs, vrp, up);
  glUniformMatrix4fv (viewLoc, 1, GL_FALSE, &View[0][0]);
}

MyGLWidget::~MyGLWidget()
{

}
