TEMPLATE    = app
QT         += opengl 

INCLUDEPATH +=  /usr/include/glm

FORMS += Abacus.ui

HEADERS += AbacusForm.h MyGLWidget.h MyLabel.h MyLCD.h

SOURCES += main.cpp \
        AbacusForm.cpp MyGLWidget.cpp MyLabel.cpp MyLCD.cpp
