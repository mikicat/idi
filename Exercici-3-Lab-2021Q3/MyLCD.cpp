#include "MyLCD.h"
// constructor
MyLCD::MyLCD(QWidget *parent) : QLCDNumber(parent) {
}

// implementació slots

void MyLCD::actualitzaNum() {
    display((intValue()+1)%64);
}

void MyLCD::resetLCD() {
    display(0);
}
