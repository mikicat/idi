#include <QLCDNumber>

class MyLCD: public QLCDNumber
{
    Q_OBJECT
public:
    MyLCD (QWidget *parent);
public slots:
    /* actualitzaNum(): actualitza el numero de la LCD */
    void actualitzaNum();
    /* resetLCD(): estableix el numero de la LCD a 0 */
    void resetLCD();
signals:
};
