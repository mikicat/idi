#include "MyLabel.h"
// constructor
MyLabel::MyLabel(QWidget *parent) : QLabel(parent) {
}

// implementació slots

void MyLabel::actualitzaText() {
    QString s;
    s = text();
    if (s == "0") {
        // Si text és 1 -> fons groc
        setText("1");    
        setStyleSheet("background: yellow; color: black");
    }
    else {
        // Si text és 0 -> fons verd
        setText("0");
        setStyleSheet("background: green; color: white");
        emit carry();
    }
}

void MyLabel::resetText() {
    setText("0");
    setStyleSheet("background: green; color: white");
}
