#include <QLabel>

class MyLabel: public QLabel
{
    Q_OBJECT
public:
    MyLabel (QWidget *parent);
public slots:
    /* actualitzaText(): canvia el valor de l'etiqueta 
     * (si antic = 0 -> actual = 1; i al revés)
     */
    void actualitzaText();
    /* resetText(): estableix el valor de l'etiqueta a 0 */
    void resetText();
signals:
    /* carry(): senyal que s'envia quan hi ha "bit de carry" en sumar en binari */
    void carry();
};
