#include <QApplication>
#include <QSurfaceFormat>
#include "AbacusForm.h"

int main (int argc, char **argv) 
{
  QApplication a(argc, argv);
  QSurfaceFormat f;
  f.setVersion(3,3);
  f.setProfile(QSurfaceFormat::CoreProfile);
  QSurfaceFormat::setDefaultFormat(f);

  AbacusForm abacusForm;
  abacusForm.show();

  return a.exec ();
}

