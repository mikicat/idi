
// MyGLWidget.cpp

#include "MyGLWidget.h"
#include <iostream>
#include <stdio.h>

#define printOpenGLError() printOglError(__FILE__, __LINE__)
#define CHECK() printOglError(__FILE__, __LINE__,__FUNCTION__)
#define DEBUG() std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::endl;

int MyGLWidget::printOglError(const char file[], int line, const char func[]) 
{
    GLenum glErr;
    int    retCode = 0;

    glErr = glGetError();
    const char * error = 0;
    switch (glErr)
    {
        case 0x0500:
            error = "GL_INVALID_ENUM";
            break;
        case 0x501:
            error = "GL_INVALID_VALUE";
            break;
        case 0x502: 
            error = "GL_INVALID_OPERATION";
            break;
        case 0x503:
            error = "GL_STACK_OVERFLOW";
            break;
        case 0x504:
            error = "GL_STACK_UNDERFLOW";
            break;
        case 0x505:
            error = "GL_OUT_OF_MEMORY";
            break;
        default:
            error = "unknown error!";
    }
    if (glErr != GL_NO_ERROR)
    {
        printf("glError in file %s @ line %d: %s function: %s\n",
                             file, line, error, func);
        retCode = 1;
    }
    return retCode;
}

MyGLWidget::~MyGLWidget() {}

/*void MyGLWidget::initializeGL() {
    LL4GLWidget::initializeGL();
}*/

void MyGLWidget::keyPressEvent(QKeyEvent *e) {
    makeCurrent();
    switch (e->key()) {
        case Qt::Key_Left:
            angleCotxe -= float(M_PI/4);
            cotxeTransform();
            break;
        case Qt::Key_Right:
            angleCotxe += float(M_PI/4);
            cotxeTransform();
            break;
        case Qt::Key_F:
            selectVal = not selectVal;
            glUniform1i(selectLoc, selectVal);
            break;
        case Qt::Key_R:
            angleCotxe = float(M_PI/4); // angle inicial
            cotxeTransform();
            selectVal = 1;
            glUniform1i(selectLoc, selectVal);
            break;
        default:
            LL4GLWidget::keyPressEvent(e);
    }
    update();
}

void MyGLWidget::setupLights() {
    LL4GLWidget::setupLights();

    selectLoc = glGetUniformLocation (program->programId(), "select");
    glUniform1i(selectLoc, selectVal);
}

void MyGLWidget::leftBtn() {
    angleCotxe -= float(M_PI/4);
    cotxeTransform();
    update();
}

void MyGLWidget::rightBtn() {
    angleCotxe += float(M_PI/4);
    cotxeTransform();
    update();
}
