#version 330 core

in vec3  fvertex;
in vec3  fnormal;
in vec3  fmatamb;
in vec3  fmatdiff;
in vec3  fmatspec;
in float fmatshin;

uniform vec3 posFocus;
uniform vec3 colFocus;

uniform mat4 TG;
uniform mat4 Proj;
uniform mat4 View;

uniform int select;

out vec4 FragColor;

vec3 posFocus1 = vec3(-3.5, 10.0, -3.5);
vec3 posFocus2 = vec3(3.5, 10.0, -3.5);
vec3 posFocus3 = vec3(3.5, 10, 3.5);

vec3 colFocus1 = vec3(0.9, 0.2, 0.2);
vec3 colFocus2 = vec3(0.0, 1.0, 0.0);
vec3 colFocus3 = vec3(0.2, 0.2, 0.9);


vec3 llumAmbient = vec3(0.1, 0.1, 0.1);

vec3 Ambient() {
  return llumAmbient*fmatamb;
}


vec3 Lambert (vec3 NormSCO, vec3 L, vec3 colFocus)
{
  // Fixeu-vos que SOLS es retorna el terme de Lambert!
  // S'assumeix que els vectors que es reben com a paràmetres estan normalitzats
  vec3 colRes = vec3(0);
  if (dot (L, NormSCO) > 0)
    colRes = colFocus * fmatdiff * dot (L, NormSCO);
  return (colRes);
}

vec3 Phong (vec3 NormSCO, vec3 L, vec3 vertSCO, vec3 colFocus)
{
  // Fixeu-vos que SOLS es retorna el terme especular!
  // Assumim que els vectors estan normalitzats
  vec3 colRes = vec3 (0);
  // Si la llum ve de darrera o el material és mate no fem res
  if ((dot(NormSCO,L) < 0) || (fmatshin == 0))
    return colRes;  // no hi ha component especular

  vec3 R = reflect(-L, NormSCO); // equival a: 2.0*dot(NormSCO,L)*NormSCO - L;
  vec3 V = normalize(-vertSCO); // perquè la càmera està a (0,0,0) en SCO

  if (dot(R, V) < 0)
    return colRes;  // no hi ha component especular

  float shine = pow(max(0.0, dot(R, V)), fmatshin);
  return (colRes + fmatspec * colFocus * shine);
}


void main()
{
    vec3 color = vec3(1);
    if (select == 1) {
        vec3 L     = normalize(posFocus - fvertex);
        color = Ambient() + 
                Lambert(normalize(fnormal), L, colFocus) + 
                Phong (normalize(fnormal), L, fvertex, colFocus);
    } else {
        vec3 L = normalize(posFocus1 - fvertex);
        vec3 color1 =
                Lambert(normalize(fnormal), L, colFocus1) +
                Phong (normalize(fnormal), L, fvertex, colFocus1);
        L = normalize(posFocus2 - fvertex);
        vec3 color2 =
                Lambert(normalize(fnormal), L, colFocus2) +
                Phong (normalize(fnormal), L, fvertex, colFocus2);
        L = normalize(posFocus3 - fvertex);
        vec3 color3 = Ambient() + 
                Lambert(normalize(fnormal), L, colFocus3) +
                Phong (normalize(fnormal), L, fvertex, colFocus3);
        color = color1 + color2 + color3;
    }
    FragColor = vec4(color, 1);
 }

