/********************************************************************************
** Form generated from reading UI file 'colorpicker.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COLORPICKER_H
#define UI_COLORPICKER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Colorpicker
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QRadioButton *red;
    QRadioButton *green;
    QRadioButton *blue;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QLabel *L_R;
    QLabel *L_G;
    QLabel *L_B;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton;

    void setupUi(QWidget *Colorpicker)
    {
        if (Colorpicker->objectName().isEmpty())
            Colorpicker->setObjectName(QStringLiteral("Colorpicker"));
        Colorpicker->resize(335, 296);
        verticalLayout_3 = new QVBoxLayout(Colorpicker);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        red = new QRadioButton(Colorpicker);
        red->setObjectName(QStringLiteral("red"));

        verticalLayout->addWidget(red);

        green = new QRadioButton(Colorpicker);
        green->setObjectName(QStringLiteral("green"));

        verticalLayout->addWidget(green);

        blue = new QRadioButton(Colorpicker);
        blue->setObjectName(QStringLiteral("blue"));

        verticalLayout->addWidget(blue);


        horizontalLayout->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        L_R = new QLabel(Colorpicker);
        L_R->setObjectName(QStringLiteral("L_R"));
        L_R->setStyleSheet(QLatin1String("background-color: red;\n"
""));

        verticalLayout_2->addWidget(L_R);

        L_G = new QLabel(Colorpicker);
        L_G->setObjectName(QStringLiteral("L_G"));
        L_G->setStyleSheet(QStringLiteral("background-color: green;"));

        verticalLayout_2->addWidget(L_G);

        L_B = new QLabel(Colorpicker);
        L_B->setObjectName(QStringLiteral("L_B"));
        L_B->setStyleSheet(QStringLiteral("background-color: blue;"));

        verticalLayout_2->addWidget(L_B);


        horizontalLayout->addLayout(verticalLayout_2);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButton = new QPushButton(Colorpicker);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout_3->addLayout(horizontalLayout_2);


        retranslateUi(Colorpicker);
        QObject::connect(red, SIGNAL(clicked(bool)), L_R, SLOT(setEnabled(bool)));
        QObject::connect(green, SIGNAL(clicked(bool)), L_G, SLOT(setEnabled(bool)));
        QObject::connect(blue, SIGNAL(clicked(bool)), L_B, SLOT(setEnabled(bool)));
        QObject::connect(red, SIGNAL(clicked(bool)), L_G, SLOT(setDisabled(bool)));
        QObject::connect(red, SIGNAL(clicked(bool)), L_B, SLOT(setDisabled(bool)));
        QObject::connect(green, SIGNAL(clicked(bool)), L_R, SLOT(setDisabled(bool)));
        QObject::connect(green, SIGNAL(clicked(bool)), L_G, SLOT(setEnabled(bool)));
        QObject::connect(green, SIGNAL(clicked(bool)), L_B, SLOT(setDisabled(bool)));
        QObject::connect(blue, SIGNAL(clicked(bool)), L_R, SLOT(setDisabled(bool)));
        QObject::connect(blue, SIGNAL(clicked(bool)), L_G, SLOT(setDisabled(bool)));
        QObject::connect(blue, SIGNAL(clicked(bool)), L_B, SLOT(setEnabled(bool)));
        QObject::connect(pushButton, SIGNAL(clicked()), Colorpicker, SLOT(close()));

        QMetaObject::connectSlotsByName(Colorpicker);
    } // setupUi

    void retranslateUi(QWidget *Colorpicker)
    {
        Colorpicker->setWindowTitle(QApplication::translate("Colorpicker", "Selector de color", nullptr));
        red->setText(QApplication::translate("Colorpicker", "Verme&ll", nullptr));
        green->setText(QApplication::translate("Colorpicker", "Verd", nullptr));
        blue->setText(QApplication::translate("Colorpicker", "Bla&u", nullptr));
        L_R->setText(QApplication::translate("Colorpicker", "VERMELL", nullptr));
        L_G->setText(QApplication::translate("Colorpicker", "VERD", nullptr));
        L_B->setText(QApplication::translate("Colorpicker", "BLAU", nullptr));
        pushButton->setText(QApplication::translate("Colorpicker", "&Sortir", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Colorpicker: public Ui_Colorpicker {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COLORPICKER_H
