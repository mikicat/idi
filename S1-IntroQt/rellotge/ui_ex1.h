/********************************************************************************
** Form generated from reading UI file 'ex1.ui'
**
** Created by: Qt User Interface Compiler version 5.12.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EX1_H
#define UI_EX1_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDial>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_rellotgeForm
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *l_h;
    QLCDNumber *lcd_min;
    QDial *dial_h;
    QVBoxLayout *verticalLayout;
    QLabel *l_m;
    QLCDNumber *lcd_h;
    QDial *dial_min;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton;

    void setupUi(QWidget *rellotgeForm)
    {
        if (rellotgeForm->objectName().isEmpty())
            rellotgeForm->setObjectName(QString::fromUtf8("rellotgeForm"));
        rellotgeForm->resize(405, 450);
        verticalLayout_3 = new QVBoxLayout(rellotgeForm);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        l_h = new QLabel(rellotgeForm);
        l_h->setObjectName(QString::fromUtf8("l_h"));

        verticalLayout_2->addWidget(l_h);

        lcd_min = new QLCDNumber(rellotgeForm);
        lcd_min->setObjectName(QString::fromUtf8("lcd_min"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lcd_min->sizePolicy().hasHeightForWidth());
        lcd_min->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(lcd_min);

        dial_h = new QDial(rellotgeForm);
        dial_h->setObjectName(QString::fromUtf8("dial_h"));
        sizePolicy.setHeightForWidth(dial_h->sizePolicy().hasHeightForWidth());
        dial_h->setSizePolicy(sizePolicy);
        dial_h->setMaximum(23);

        verticalLayout_2->addWidget(dial_h);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        l_m = new QLabel(rellotgeForm);
        l_m->setObjectName(QString::fromUtf8("l_m"));

        verticalLayout->addWidget(l_m);

        lcd_h = new QLCDNumber(rellotgeForm);
        lcd_h->setObjectName(QString::fromUtf8("lcd_h"));
        sizePolicy.setHeightForWidth(lcd_h->sizePolicy().hasHeightForWidth());
        lcd_h->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(lcd_h);

        dial_min = new QDial(rellotgeForm);
        dial_min->setObjectName(QString::fromUtf8("dial_min"));
        sizePolicy.setHeightForWidth(dial_min->sizePolicy().hasHeightForWidth());
        dial_min->setSizePolicy(sizePolicy);
        dial_min->setMaximum(59);

        verticalLayout->addWidget(dial_min);


        horizontalLayout->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButton = new QPushButton(rellotgeForm);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout_3->addLayout(horizontalLayout_2);


        retranslateUi(rellotgeForm);
        QObject::connect(dial_h, SIGNAL(valueChanged(int)), lcd_min, SLOT(display(int)));
        QObject::connect(dial_min, SIGNAL(valueChanged(int)), lcd_h, SLOT(display(int)));
        QObject::connect(pushButton, SIGNAL(pressed()), rellotgeForm, SLOT(close()));

        QMetaObject::connectSlotsByName(rellotgeForm);
    } // setupUi

    void retranslateUi(QWidget *rellotgeForm)
    {
        rellotgeForm->setWindowTitle(QApplication::translate("rellotgeForm", "Form", nullptr));
        l_h->setText(QApplication::translate("rellotgeForm", "Hours", nullptr));
        l_m->setText(QApplication::translate("rellotgeForm", "Minutes", nullptr));
        pushButton->setText(QApplication::translate("rellotgeForm", "&Sortir", nullptr));
    } // retranslateUi

};

namespace Ui {
    class rellotgeForm: public Ui_rellotgeForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EX1_H
