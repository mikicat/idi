TEMPLATE    = app
QT         += opengl 

INCLUDEPATH +=  /usr/include/glm

FORMS += Form.ui

HEADERS += MyForm.h MyLineEdit.h MyGLWidget.h MyLabel.h

SOURCES += main.cpp \
        MyForm.cpp MyLineEdit.cpp MyGLWidget.cpp MyLabel.cpp
