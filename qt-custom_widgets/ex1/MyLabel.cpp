#include "MyLabel.h"
// constructor
MyLabel::MyLabel(QWidget *parent) : QLabel(parent) {
    // Inicialització d'atributs, si cal
}

// implementació slots

void MyLabel::truncaText(int i) {
    QString s;
    s = text();
    s.truncate(i);
    //emit returnPressed(s);
    setText(s);
}
