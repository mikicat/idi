#include "MyLineEdit.h"
// constructor
MyLineEdit::MyLineEdit(QWidget *parent) : QLineEdit(parent) {
    connect(this, SIGNAL(returnPressed()), this, SLOT(tractaReturn()));
    // Inicialització d'atributs, si cal
}

// implementació slots
// sobra
void MyLineEdit::tractaReturn() {
}
void MyLineEdit::truncaText(int i) {
    QString s;
    s = text();
    s.truncate(i);
    emit returnPressed(s);
}
