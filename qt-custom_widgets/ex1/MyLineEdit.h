#include <QLineEdit>

class MyLineEdit: public QLineEdit
{
    Q_OBJECT
public:
    MyLineEdit (QWidget *parent);
public slots:
    void tractaReturn();
    void truncaText(int i);
signals:
    void returnPressed (const QString &);
};

