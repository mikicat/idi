TEMPLATE    = app
QT         += opengl 

INCLUDEPATH +=  /usr/include/glm

FORMS += form.ui

HEADERS += MyForm.h MyLineEdit.h MyGLWidget.h

SOURCES += main.cpp \
        MyForm.cpp MyLineEdit.cpp MyGLWidget.cpp
