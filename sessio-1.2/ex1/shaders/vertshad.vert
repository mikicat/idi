#version 330 core

in vec3 vertex;
out vec2 pos;

void main()  {
    gl_Position = vec4 (vertex, 1.0);
    pos = vertex.xy;
}
