#version 330 core

out vec4 FragColor;
in vec2 pos;

void main() {
    if (int(gl_FragCoord.y)%20 <= 10) discard;
    FragColor = vec4(1.);
    if (pos.x < 0.0) {
      if (pos.y < 0.0) {
        FragColor = vec4(1., 1., 0., 1);
      }
      else {
        FragColor = vec4(1., 0., 0., 1);
      }
    }
    else {
      if (pos.y < 0.0) {
        FragColor = vec4(0., 1., 0., 1);
      }
      else {
        FragColor = vec4(0., 0., 1., 1);
      }
    }
}
