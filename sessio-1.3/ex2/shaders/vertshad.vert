#version 330 core

in vec3 vertex;

in vec3 color;
out vec4 c;

uniform float val = 1;
uniform mat4 TG;


void main()  {
    gl_Position = TG * vec4 (vertex*val, 1.0);
    c = vec4(color, 1.0);
}
