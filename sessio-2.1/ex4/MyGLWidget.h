// MyGLWidget.h
#include "Bl2GLWidget.h"
#include "models/model.h"

class MyGLWidget : public Bl2GLWidget {
  Q_OBJECT

  public:
    MyGLWidget(QWidget *parent=0) : Bl2GLWidget(parent) {}
    ~MyGLWidget();

  private:
    int printOglError(const char file[], int line, const char func[]);

  protected:
    virtual void carregaShaders();
    virtual void initializeGL();
    virtual void creaBuffers();
    virtual void paintGL();
    virtual void keyPressEvent (QKeyEvent *event);
    void ini_camera();
    void projectTransform();
    void viewTransform();
    void modelTransformHomer();
    void modelTransformTerra();

    GLuint projLoc; // projection Location
    GLuint viewLoc; // view matrix Location
    GLuint VAO_Homer; // VAO del model de homer
    GLuint VAO_Terra; // VAO del Terra
    Model m; // un únic model
    float FOV, ra, zNear, zFar, angle;
    glm::vec3 OBS, VRP, UP;
};
