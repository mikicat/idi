// MyGLWidget.cpp
#include "MyGLWidget.h"
#include <iostream>
#include <stdio.h>

#define printOpenGLError() printOglError(__FILE__, __LINE__)
#define CHECK() printOglError(__FILE__, __LINE__,__FUNCTION__)
#define DEBUG() std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::endl;

int MyGLWidget::printOglError(const char file[], int line, const char func[])
{
    GLenum glErr;
    int    retCode = 0;

    glErr = glGetError();
    const char * error = 0;
    switch (glErr)
    {
        case 0x0500:
            error = "GL_INVALID_ENUM";
            break;
        case 0x501:
            error = "GL_INVALID_VALUE";
            break;
        case 0x502:
            error = "GL_INVALID_OPERATION";
            break;
        case 0x503:
            error = "GL_STACK_OVERFLOW";
            break;
        case 0x504:
            error = "GL_STACK_UNDERFLOW";
            break;
        case 0x505:
            error = "GL_OUT_OF_MEMORY";
            break;
        default:
            error = "unknown error!";
    }
    if (glErr != GL_NO_ERROR)
    {
        printf("glError in file %s @ line %d: %s function: %s\n",
                             file, line, error, func);
        retCode = 1;
    }
    return retCode;
}

void MyGLWidget::calculaCapsa() {
  minEscena = glm::vec3(-2,-1,-2);
  maxEscena = glm::vec3(2,1,2);
  centreEscena = (minEscena + maxEscena) / glm::vec3(2.);
  rEscena = distance(centreEscena, minEscena);
}

void MyGLWidget::carregaShaders() {
  Bl2GLWidget::carregaShaders(); // Cridem primer al mètode de BL2GLWidget
  projLoc = glGetUniformLocation(program->programId(), "proj");
  viewLoc = glGetUniformLocation(program->programId(), "view");
  ini_camera();
}

void MyGLWidget::creaBuffers() {
  m.load("../Model/HomerProves.obj");
  glGenVertexArrays(1, &VAO_Homer);
  glBindVertexArray(VAO_Homer);

  GLuint VBO_HomerProves[2];
  glGenBuffers(2, VBO_HomerProves);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_HomerProves[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*m.faces().size()*3*3, m.VBO_vertices(), GL_STATIC_DRAW);
  // Activem l'atribut vertexLoc
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(vertexLoc);

  glBindBuffer(GL_ARRAY_BUFFER, VBO_HomerProves[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*m.faces().size()*3*3, m.VBO_matdiff(), GL_STATIC_DRAW);
  // Activem l'atribut colorLoc
  glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(colorLoc);

  // Terra

  glm::vec3 pos_terra[6] = {
      glm::vec3(2.0, -1.0, 2.0),
      glm::vec3(2.0, -1.0, -2.0),
      glm::vec3(-2.0, -1.0, -2.0),

      glm::vec3(2.0, -1.0, 2.0),
      glm::vec3(-2.0, -1.0, -2.0),
      glm::vec3(-2.0, -1.0, 2.0)
  };

  glm::vec3 col_terra[6] = {
    glm::vec3(1.0, 0.0, 0.0),
    glm::vec3(0.0, 1.0, 0.0),
    glm::vec3(0.0, 0.0, 1.0),
    glm::vec3(1.0, 0.0, 0.0),
    glm::vec3(0.0, 0.0, 1.0),
    glm::vec3(0.0, 1.0, 0.0)
  };

  glGenVertexArrays(2, &VAO_Terra);
  glBindVertexArray(VAO_Terra);

  GLuint VBO_Terra[2];
  glGenBuffers(2, VBO_Terra);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_Terra[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(pos_terra), pos_terra, GL_STATIC_DRAW);
  // Activar vertex
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(vertexLoc);

  glBindBuffer(GL_ARRAY_BUFFER, VBO_Terra[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(col_terra), col_terra, GL_STATIC_DRAW);
  // Activar colors
  glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(colorLoc);

  glBindVertexArray(0);
}

void MyGLWidget::ini_camera() {
  calculaCapsa();
  float d = rEscena * 2;
  FOV = 2*asin(rEscena/d);
  iniFOV = FOV;
  raw = 1.0;
  zNear = rEscena;
  zFar = rEscena + d;
  projectTransform();

  OBS = glm::vec3(0,0,d);
  VRP = glm::vec3(0,0,0);
  UP = glm::vec3(0,1,0);

  viewTransform();
}

void MyGLWidget::initializeGL() {
  Bl2GLWidget::initializeGL();
  glEnable (GL_DEPTH_TEST);
}

void MyGLWidget::keyPressEvent(QKeyEvent* event) {
  makeCurrent();
  switch (event->key()) {
    case Qt::Key_S: { // escalar a més gran
      scale += 0.05;
      break;
    }
    case Qt::Key_D: { // escalar a més petit
      scale -= 0.05;
      break;
    }
    case Qt::Key_R: {
      angle += float(M_PI/4);
      break;
    }
    default: event->ignore(); break;
  }
  update();
}

void MyGLWidget::modelTransformHomer ()
{
  // Matriu de transformació de model
  glm::mat4 transform (1.0f);
  transform = transform * glm::rotate(transform, angle, glm::vec3(0,1,0));
  transform = glm::scale(transform, glm::vec3(scale));
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &transform[0][0]);
}

void MyGLWidget::modelTransformTerra ()
{
  // Matriu de transformació de model
  glm::mat4 transform (1.0f);
  transform = glm::scale(transform, glm::vec3(scale));
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &transform[0][0]);
}

void MyGLWidget::paintGL() {
  //Bl2GLWidget::paintGL();
  glViewport(0, 0, width(), height()); // no caldria, Qt ho fa automàticament

  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  modelTransformHomer ();
  glBindVertexArray (VAO_Homer);
  glDrawArrays(GL_TRIANGLES, 0, sizeof(GLfloat)*m.faces().size()*3);

  modelTransformTerra();
  glBindVertexArray (VAO_Terra);
  glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray(0);
}

void MyGLWidget::projectTransform() {
  // glm::perspective (FOV en rad, ra window, znear, zfar);
  glm::mat4 Proj = glm::perspective (FOV, raw, zNear, zFar);
  glUniformMatrix4fv (projLoc, 1, GL_FALSE, &Proj[0][0]);
}

void MyGLWidget::resizeGL (int w, int h) {
  float rav = float(w) / float(h);
  raw = rav;
  if (rav < 1.0) FOV = 2.0*atan(tan(iniFOV/2.0)/rav);
  projectTransform();
  glViewport(0, 0, w, h);
}

void MyGLWidget::viewTransform() {
  // glm::lookAt (OBS, VRP, UP)
  glm::mat4 View = glm::lookAt(OBS, VRP, UP);
  glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &View[0][0]);
}

MyGLWidget::~MyGLWidget() {
}
