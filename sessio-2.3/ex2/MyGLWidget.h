// MyGLWidget.h
#include "Bl2GLWidget.h"
#include "models/model.h"

class MyGLWidget : public Bl2GLWidget {
  Q_OBJECT

  public:
    MyGLWidget(QWidget *parent=0) : Bl2GLWidget(parent) {}
    ~MyGLWidget();

  private:
    int printOglError(const char file[], int line, const char func[]);

  protected:
    virtual void carregaShaders();
    virtual void creaBuffers();
    virtual void initializeGL();
    virtual void keyPressEvent (QKeyEvent *event);
    virtual void paintGL();
    virtual void resizeGL (int w, int h);
    virtual void mouseMoveEvent (QMouseEvent *event);

    void calculaCapsaEscena();
    void calculaCapsaModel();
    void ini_camera();
    void modelTransformPatricio();
    void modelTransformTerra();
    void projectTransform();
    void viewTransform();


    GLuint projLoc; // projection Location
    GLuint viewLoc; // view matrix Location
    GLuint VAO_Patricio; // VAO del model de Patricio
    GLuint VAO_Terra; // VAO del Terra
    Model m; // un únic model
    float FOV, iniFOV, raw, zNear, zFar;
    float angle = 0; // angle rotacio Y Patricio
    glm::vec3 OBS, VRP, UP;

    // Càlcul capsa escena
    glm::vec3 minEscena, maxEscena, centreEscena;
    float rEscena, d;

    // Càlcul capsa model
    glm::vec3 centreBasePatricio;
    float escala;

    // Variables camera ortogonal
    float l, r, b, t;
    bool orto = false;

    // Declarar angles d'Euler
    float theta, psi;
    float x_ant, y_ant; // x i y del ratolí
};
