#version 330 core

in vec3 vertex;
in vec3 normal;

in vec3 matamb;
in vec3 matdiff;
in vec3 matspec;
in float matshin;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 TG;

out vec4 vertexSCO;
out vec4 focusSCO;
out vec3 NormSCO;
out vec3 matamb2;
out vec3 matdiff2;
out vec3 matspec2;
out float matshin2;


void main()
{	
    matamb2 = matamb;
    matdiff2 = matdiff;
    matspec2 = matspec;
    matshin2 = matshin;

    vertexSCO = view * TG * vec4(vertex, 1.0);
    mat3 NormalMatrix = inverse (transpose (mat3(view*TG)));
    NormSCO = normalize(vec3(NormalMatrix*normal));
    gl_Position = proj * view * TG * vec4 (vertex, 1.0);
}
