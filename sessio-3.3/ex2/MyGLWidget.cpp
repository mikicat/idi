// MyGLWidget.cpp
#include "MyGLWidget.h"
#include <iostream>
#include <stdio.h>

#define printOpenGLError() printOglError(__FILE__, __LINE__)
#define CHECK() printOglError(__FILE__, __LINE__,__FUNCTION__)
#define DEBUG() std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::endl;

int MyGLWidget::printOglError(const char file[], int line, const char func[]) 
{
    GLenum glErr;
    int    retCode = 0;

    glErr = glGetError();
    const char * error = 0;
    switch (glErr)
    {
        case 0x0500:
            error = "GL_INVALID_ENUM";
            break;
        case 0x501:
            error = "GL_INVALID_VALUE";
            break;
        case 0x502: 
            error = "GL_INVALID_OPERATION";
            break;
        case 0x503:
            error = "GL_STACK_OVERFLOW";
            break;
        case 0x504:
            error = "GL_STACK_UNDERFLOW";
            break;
        case 0x505:
            error = "GL_OUT_OF_MEMORY";
            break;
        default:
            error = "unknown error!";
    }
    if (glErr != GL_NO_ERROR)
    {
        printf("glError in file %s @ line %d: %s function: %s\n",
                             file, line, error, func);
        retCode = 1;
    }
    return retCode;
}
void MyGLWidget::initializeGL() {
  // Cal inicialitzar l'ús de les funcions d'OpenGL
  initializeOpenGLFunctions();  

  glClearColor(0.5, 0.7, 1.0, 1.0); // defineix color de fons (d'esborrat)
  posPatr = glm::vec3(0, 0, 0);
  glEnable(GL_DEPTH_TEST);
  carregaShaders();
  createBuffersPatricio();
  createBuffersTerraIParet();

  backFace = true;
  glEnable(GL_CULL_FACE);
  iniEscena();
  iniCamera();
  parametresLlum();
}

void MyGLWidget::iniMaterialTerra() {
  // Donem valors al material del terra
  amb = glm::vec3(0.2,0,0.2);
  diff = glm::vec3(0,0,0.8);
  spec = glm::vec3(0,0,0);
  shin = 128;
}
void MyGLWidget::parametresLlum() {
    glUniform3f(colFocus, 0.8, 0.8, 0.0);
    glm::vec3 glposFocus = glm::vec3(posPatr.x, 0.5, posPatr.z);
    glposFocus = glm::vec3(View * glm::vec4(glposFocus, 1.));
    glUniform3fv(posFocus, 1, &glposFocus[0]);  // en SCA
}

MyGLWidget::~MyGLWidget() {
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *e)
{
  makeCurrent();
  // Aqui cal que es calculi i s'apliqui la rotacio o el zoom com s'escaigui...
  if (DoingInteractive == ROTATE)
  {
    // Fem la rotació
    angleY += (e->x() - xClick) * M_PI / ample;
    viewTransform ();
  }

  xClick = e->x();
  yClick = e->y();

  update ();
}

void MyGLWidget::keyPressEvent(QKeyEvent* event) {
  makeCurrent();
  switch (event->key()) {
    /*case Qt::Key_K: 
      posFocusX += 0.5;
      parametresLlum();
      break;
    case Qt::Key_L:
      posFocusX -= 0.5;
      parametresLlum();
      break;*/
    case Qt::Key_F:
      focusFix = not focusFix;
      if (not focusFix) glUniform3f(posFocus, 0, 0, 1);
      else parametresLlum();
      break;
    case Qt::Key_Right:
      posPatr.x += 1;
      break;
    case Qt::Key_Left:
      posPatr.x -= 1;
      break;
    case Qt::Key_Up:
      posPatr.z += 1;
      break;
    case Qt::Key_Down:
      posPatr.z -= 1;
      break;
    case Qt::Key_B:
      backFace = not backFace;
      if (backFace) glEnable(GL_CULL_FACE);
      else glDisable(GL_CULL_FACE);
      break;
    case Qt::Key_L:
      disabled = not disabled;
      if (disabled) {
          glUniform3f(colFocus, 0, 0, 0);
      } else {
          glUniform3f(colFocus, 0.8, 0.8, 0.0);
      }
      break;
    default: Bl3GLWidget::keyPressEvent(event); break;
  }
  update();
}

void MyGLWidget::carregaShaders() {
   Bl3GLWidget::carregaShaders(); 
   colFocus = glGetUniformLocation (program->programId(), "colFocus");
   posFocus = glGetUniformLocation (program->programId(), "posFocus");
}

void MyGLWidget::modelTransformPatricio ()
{
  TG = glm::scale(glm::mat4(1.f), glm::vec3(escala, escala, escala));
  TG = glm::translate(TG, glm::vec3(posPatr.x, posPatr.y, posPatr.z));
  TG = glm::translate(TG, -centrePatr);
  
  glUniformMatrix4fv (transLoc, 1, GL_FALSE, &TG[0][0]);
}

void MyGLWidget::calculaCapsaModel ()
{
  // Càlcul capsa contenidora i valors transformacions inicials
  float minx, miny, minz, maxx, maxy, maxz;
  minx = maxx = patr.vertices()[0];
  miny = maxy = patr.vertices()[1];
  minz = maxz = patr.vertices()[2];
  for (unsigned int i = 3; i < patr.vertices().size(); i+=3)
  {
    if (patr.vertices()[i+0] < minx)
      minx = patr.vertices()[i+0];
    if (patr.vertices()[i+0] > maxx)
      maxx = patr.vertices()[i+0];
    if (patr.vertices()[i+1] < miny)
      miny = patr.vertices()[i+1];
    if (patr.vertices()[i+1] > maxy)
      maxy = patr.vertices()[i+1];
    if (patr.vertices()[i+2] < minz)
      minz = patr.vertices()[i+2];
    if (patr.vertices()[i+2] > maxz)
      maxz = patr.vertices()[i+2];
  }
  escala = 0.3/(maxy-miny);
  centrePatr[0] = (minx+maxx)/2.0; centrePatr[1] = (miny+maxy)/2.0; centrePatr[2] = (minz+maxz)/2.0;
}
